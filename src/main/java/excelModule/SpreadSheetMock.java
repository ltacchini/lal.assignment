package excelModule;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import core.DataList;

public class SpreadSheetMock implements Iterable<DataList> {

	@Override
	public Iterator<DataList> iterator() {
		List<DataList> lines = new ArrayList<>();
		List<String> line = new ArrayList<>();

		line.add("Nombre");
		line.add("Edad");
		line.add("DNI");
		lines.add(new DataList(line));

		line = new ArrayList<>();

		for (int i = 0; i < 3; i++)
			line.add(String.valueOf(i));

		lines.add(new DataList(line));
		return lines.iterator();
	}

}
