package fieldType;

import java.util.List;
import java.util.Map;

import core.DataList;
import enums.DataType;

public class TypeParser {
	private List<DataList> lines;
	private FieldTypeFiller ff;
	private FieldTypeAssigner fa;
	private IterableManipulator manipulator = new IterableManipulator();

	public TypeParser(Iterable<DataList> dataLists, FieldTypeFiller ff, FieldTypeAssigner fa ) {
		this.lines = manipulator.iterableToList(dataLists);
		this.fa = fa;
		this.ff = ff;
	}

	public Map<String, DataType> getTypes() {
		Map<String, DataType> types1 = ff.fill(lines);
		Map<String, DataType> types2 = fa.fill(lines);

		for (String key : types2.keySet()) {
			if (!types1.containsKey(key)) {
				types1.put(key, types2.get(key));
			}
		}
		return types1;
	}

}
