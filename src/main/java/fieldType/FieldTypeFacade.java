package fieldType;

import java.util.Map;

import config.Config;
import core.DataList;
import enums.DataType;
import handler.HandlerFacade;

public class FieldTypeFacade {

	private TypeParser typeParser;

	public FieldTypeFacade(Config config, Iterable<DataList> dataList) {
		this.typeParser = new TypeParser(dataList, new FieldTypeFiller(config),
						  new FieldTypeAssigner(new HandlerFacade()));
	}

	public Map<String, DataType> getTypes() {
		return typeParser.getTypes();
	}

}
