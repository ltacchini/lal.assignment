package fieldType;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import config.Config;
import core.DataList;
import enums.DataType;

public class FieldTypeFiller {

	private Config config;

	public FieldTypeFiller(Config config) {
		this.config = config;
	}

	public Map<String, DataType> fill(Iterable<DataList> lines) {
		IterableManipulator manipulator = new IterableManipulator();
		List<DataList> l = manipulator.iterableToList(lines);

		DataList line;
		try {
			line = l.get(0);
		} catch (Exception e) {
			throw new NoSuchElementException(e.getMessage());
		}

		Map<String, DataType> dataTypes = new HashMap<String, DataType>();
		String field;
		for (String key : line) {
			field = config.getElement(key);
			if (field != null)
				dataTypes.put(key, DataType.parse(field));
		}
		return dataTypes;
	}

	// ?.
	public DataType getDataType(String key, Map<String, DataType> dataTypes) throws IOException {
		if (!dataTypes.containsKey(key) || dataTypes.get(key) == null)
			throw new IOException("Not found");
		return dataTypes.get(key);
	}
}
