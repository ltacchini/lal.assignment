package fieldType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import core.DataList;
import enums.DataType;
import handler.Handler;

public class FieldTypeAssigner {

	private Handler handler;

	public FieldTypeAssigner(Handler handler) {
		this.handler = handler;
	}

	public Map<String, DataType> fill(Iterable<DataList> lines) {
		IterableManipulator manipulator = new IterableManipulator();
		
		List<DataList> listLines = manipulator.iterableToList(lines);

		List<String> keys = manipulator.getLine(listLines, 0);
		List<String> values = manipulator.getLine(listLines, 1);

		Map<String, DataType> columnToType = new HashMap<String, DataType>();
		for (int i = 0; i < keys.size(); i++) {			
			columnToType.put(keys.get(i), handler.handle(values.get(i)));
		}
		return columnToType;
	}

}