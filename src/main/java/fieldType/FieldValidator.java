package fieldType;

import java.util.Map;

import enums.DataType;

public class FieldValidator {

	FieldTypeFiller filler;

	public FieldValidator(FieldTypeFiller filler) {
		this.filler = filler;
	}

	public void validate(String key, DataType type, Map<String, DataType> map) throws Exception {
		if (!(filler.getDataType(key, map)).equals(type))
			throw new Exception("Wrong DataType for this key");
	}

}
