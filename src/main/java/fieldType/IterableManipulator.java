package fieldType;

import java.util.ArrayList;
import java.util.List;

import core.DataList;

public class IterableManipulator {

	public List<DataList> iterableToList(Iterable<DataList> lines) {
		List<DataList> listLines = new ArrayList<>();
		lines.iterator().forEachRemaining(listLines::add);
		return listLines;
	}

	public List<String> getLine(List<DataList> listLines, int i) {
		List<String> row = new ArrayList<String>();
		for (String value : listLines.get(i)) {
			row.add(value);
		}
		return row;
	}

}
