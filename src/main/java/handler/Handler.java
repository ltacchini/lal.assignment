package handler;

import enums.DataType;

public interface Handler {

	DataType handle(Object o);
	
}
