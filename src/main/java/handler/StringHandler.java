package handler;

import enums.DataType;

public class StringHandler implements Handler {

	private DataType type = DataType.STRING;
	private Handler nextHandler;

	protected StringHandler(Handler nextHandler) {
		this.nextHandler = nextHandler;
	}

	@Override
	public DataType handle(Object o) {
		try {
			@SuppressWarnings("unused")
			String ret = (String) o;
			return type;
		} catch (Exception e) {
			if (nextHandler != null) {
				return nextHandler.handle(o);
			}
		}
		return null;
	}

}
