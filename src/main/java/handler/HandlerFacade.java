package handler;

import enums.DataType;

public class HandlerFacade implements Handler {

	@Override
	public DataType handle(Object o) {
		Handler strHandler = new StringHandler(null);
		Handler intHandler = new IntHandler(strHandler);
		
		return intHandler.handle(o);
	}

}
