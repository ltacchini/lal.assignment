package config;

public class MockConfig implements Config {
	
	@Override
	public String getElement(String key) {
		if(key.equals("Edad"))
			return null;
		if(key.equals("Nombre"))
			return "String";
		return "Edad";
	}

	@Override
	public String getKeyColumnas() {
		return "Columnas";
	}


}
