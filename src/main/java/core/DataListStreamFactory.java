package core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.eval.NotImplementedException;

import excelModule.SpreadSheet;

public class DataListStreamFactory {

	public Iterable<DataList> create(String type, String filePath)
			throws NotImplementedException, EncryptedDocumentException, InvalidFormatException, IOException {
		final String spreadSheeta = "xlsx";
		final String spreadSheetb = "xls";

		switch (type) {
		case spreadSheeta:
		case spreadSheetb:
			ArrayList<String> pathArr = new ArrayList<>(Arrays.asList(filePath.split("/")));
			String sheet = pathArr.get(pathArr.size() - 1);
			pathArr.remove(pathArr.size() - 1);
			String path = String.join("/", pathArr);
			return new SpreadSheet(path, sheet);
		default:
			throw new NotImplementedException("Can't read that type");
		}
	}

}