package filter;

import java.util.ArrayList;
import java.util.List;

import core.DataList;
import fieldType.IterableManipulator;

public class FieldFilter {

	private IterableManipulator itm;

	public FieldFilter() {
		this.itm = new IterableManipulator();
	}

	public List<DataList> filter(Iterable<DataList> lines, List<String> neededFields) {
		List<DataList> ret = new ArrayList<>();

		List<DataList> target = itm.iterableToList(lines);

		List<Integer> index = new ArrayList<>();
		int aux = 0;
		for (String s : target.get(0)) {
			for (String nf : neededFields) {
				if (s.toLowerCase().equals(nf.toLowerCase())) {
					index.add(aux);
					break;
				}
			}
			aux++;
		}

		for (DataList l : target) {
			List<String> values = new ArrayList<>();
			for (String value : l) {
				values.add(value);
			}
			List<String> newLine = new ArrayList<>();
			for (int i : index) {
				if (values.isEmpty())
					break;
				newLine.add(values.get(i));
			}
			ret.add(new DataList(newLine));
		}
		return ret;
	}
}