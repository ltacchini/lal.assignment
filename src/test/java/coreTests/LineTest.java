package coreTests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import core.DataList;

public class LineTest {

	private List<String> words;
	
	@Before 
	public void setUp() {
		words = new ArrayList<>();
		for(int i = 0; i < 10; i++) {
			words.add(String.valueOf(i));
		}
	}
	
	@Test
	public void testIterator() {
		DataList line = new DataList(words);
		Iterator<String> itr = line.iterator();
		assertEquals("0", itr.next());
	}

}
