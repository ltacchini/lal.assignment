package coreTests;

import static org.junit.Assert.fail;

import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import core.DataListStreamFactory;

public class LineStreamFactoryTest {
	
	private DataListStreamFactory factory;
	private String path;

	@Before
	public void setUp() throws Exception {
		path = "resources/hojaSimple.xlsx/Hoja 1";
		factory = new DataListStreamFactory();
	}

	@Test
	public void testCreate() {
		try {
			factory.create("xlsx", path);
		} catch (Exception e) {
			fail();
		}
	}

	@Test(expected = NotImplementedException.class)
	public void testCreateNotImplemented() throws Exception {
		factory.create("txt", path);
	}

}
