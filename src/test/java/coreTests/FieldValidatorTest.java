package coreTests;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import config.DefaultConfig;
import core.DataListStreamFactory;
import enums.DataType;
import excelModule.SpreadSheet;
import fieldType.FieldTypeFiller;
import fieldType.FieldValidator;

public class FieldValidatorTest {

	DefaultConfig columnasTipo;
	DefaultConfig columnasTipoError;
	DefaultConfig archivoHojaSimple; 
	
	FieldTypeFiller instanciaErrorEdad;
	FieldTypeFiller instanciaErrorNombre;
	
	FieldValidator validator;
	
	SpreadSheet hojaSimpleErrEdad;
	SpreadSheet hojaSimpleErrNombre; 

	@Before 
	public void setUp() throws IOException, EncryptedDocumentException, NotImplementedException, InvalidFormatException {
		columnasTipo = new DefaultConfig("columnasTipo.properties");
		columnasTipoError = new DefaultConfig("columnasTipoError.properties");
		
		archivoHojaSimple = new DefaultConfig("TestConfig.properties");	
		
		DataListStreamFactory lsf = new DataListStreamFactory();
		
		hojaSimpleErrEdad = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathErrorEdad"));
		instanciaErrorEdad = new FieldTypeFiller(columnasTipo);
		
		hojaSimpleErrNombre = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathErrorNombre"));
		instanciaErrorNombre = new FieldTypeFiller(columnasTipo);
		
		validator = new FieldValidator(instanciaErrorEdad);		
	}
	
	
	@Test (expected = Exception.class)
	public void testA() throws Exception {
		validator.validate("Edad", DataType.STRING,instanciaErrorEdad.fill(hojaSimpleErrEdad));
	}	
	
	@Test (expected = Exception.class)
	public void testB() throws Exception {
		validator.validate("Nombre", DataType.INTEGER,instanciaErrorEdad.fill(hojaSimpleErrNombre));
	}	

}
