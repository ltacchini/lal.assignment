package filterTests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import config.Config;
import config.MockConfig;
import core.DataList;
import excelModule.SpreadSheetMock;
import filter.FieldFilter;

public class ColumnFilterTest {
	private FieldFilter cFilter;
	private Config config;
	private List<String> neededFields;
	private Iterable<DataList> ss;

	@Before
	public void setUp() throws Exception {
		config = new MockConfig();

		neededFields = Arrays.asList(config.getElement("Columnas").split(";"));
		cFilter = new FieldFilter();
		ss = new SpreadSheetMock();	
	}

	@Test
	public void testFilter() {
		Iterator<String> itr = cFilter.filter(ss, neededFields).iterator().next().iterator();
		assertEquals("Edad", itr.next());
		assertEquals(false, itr.hasNext());
	}

	public void testValues() {
		Iterator<DataList> itr = cFilter.filter(ss, neededFields).iterator();
		itr.next();
		assertEquals("1", itr.next());
		assertEquals(false, itr.hasNext());
	}	

}
