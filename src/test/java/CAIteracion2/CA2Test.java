package CAIteracion2;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import core.DataList;
import enums.DataType;
import excelModule.SpreadSheetMock;
import fieldType.FieldTypeAssigner;
import handler.Handler;
import handler.HandlerFacade;

public class CA2Test {

	private List<DataList> lines;
	private Handler iHandler;
	private FieldTypeAssigner parser;

	@Before
	public void setUp() throws Exception {
		lines = new ArrayList<>();
		Iterable<DataList> spreadSheet = new SpreadSheetMock();
		spreadSheet.iterator().forEachRemaining(lines::add);
		parser = new FieldTypeAssigner(new HandlerFacade());
		iHandler = new HandlerFacade();
	}

	@Test
	public void testa() {
		// Se lee la primera fila de “SpreadSheetMock”
		// Se lee el valor “Nombre” y es de tipo String.
		// Se lee el valor “Edad” y es de tipo String.
		// Se lee el valor “Dni” y es de tipo String.
		DataList line = lines.get(0);
		for (Object field : line) {
			assertEquals(DataType.STRING, iHandler.handle(field));
		}
	}

	@Test
	public void testb() {
		// Se lee la segunda fila de “SpreadSheetMock”
		// Se lee el valor 0 y es de tipo Integer.
		// Se lee el valor 1 y es de tipo Integer.
		// Se lee el valor 2 y es de tipo Integer.
		DataList line = lines.get(1);
		for (Object field : line) {
			assertEquals(DataType.INTEGER, iHandler.handle(field));
		}
	}

	@Test
	// Se leen datos de un mock de “SpreadSheetMock” con la clase Parser
	// Se pide el valor de la columna “DNI” se devuelve que es de tipo Integer.
	public void testc() {
		assertEquals(DataType.INTEGER, parser.fill(lines).get("DNI"));
	}

}
