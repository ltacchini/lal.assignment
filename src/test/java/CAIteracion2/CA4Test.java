package CAIteracion2;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import config.DefaultConfig;
import core.DataListStreamFactory;
import enums.DataType;
import excelModule.SpreadSheet;
import fieldType.FieldTypeAssigner;
import fieldType.FieldTypeFiller;
import fieldType.TypeParser;
import handler.Handler;
import handler.HandlerFacade;

public class CA4Test {

	private DefaultConfig columnasTipo;
	private DefaultConfig archivoHojaSimple;

	private TypeParser instancia1;
	private TypeParser instancia2;

	private SpreadSheet hojaSimple;

	@Before
	public void setUp()
			throws IOException, EncryptedDocumentException, NotImplementedException, InvalidFormatException {
		columnasTipo = new DefaultConfig("columnasTipo.properties");
		archivoHojaSimple = new DefaultConfig("TestConfig.properties");

		DataListStreamFactory lsf = new DataListStreamFactory();

		hojaSimple = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathHojaSimple"));
		FieldTypeAssigner fa = new FieldTypeAssigner(new HandlerFacade());
		FieldTypeFiller ff = new FieldTypeFiller(columnasTipo);
		instancia1 = new TypeParser(hojaSimple, ff, fa);
	}

	@Test
	public void test1() {
		Map<String, DataType> map = instancia1.getTypes();
		// Se utiliza el archivo de configuración “archivoTest.properties”,
		// se lee la “Hoja 1” del archivo “hojaSimple.xlsx”,
		// y se establece un String para nombre y un Integer para Edad.
		assertEquals(DataType.STRING, map.get("Nombre"));
		assertEquals(DataType.INTEGER, map.get("Edad"));
	}

	@Test
	public void test2()
			throws EncryptedDocumentException, NotImplementedException, InvalidFormatException, IOException {
		DataListStreamFactory lsf = new DataListStreamFactory();
		SpreadSheet hojaDireccion = (SpreadSheet) lsf.create("xlsx",
				archivoHojaSimple.getElement("pathHoja_con_direccion"));
		Handler df = new HandlerFacade();
		FieldTypeAssigner fa = new FieldTypeAssigner(df);
		FieldTypeFiller ff = new FieldTypeFiller(columnasTipo);
		instancia2 = new TypeParser(hojaDireccion, ff, fa);
		Map<String, DataType> map = instancia2.getTypes();

		assertEquals(DataType.STRING, map.get("Nombre"));
		assertEquals(DataType.INTEGER, map.get("Edad"));
		assertEquals(DataType.STRING, map.get("Direccion"));
	}
}
