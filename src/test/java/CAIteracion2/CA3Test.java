package CAIteracion2;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import config.DefaultConfig;
import core.DataListStreamFactory;
import enums.DataType;
import excelModule.SpreadSheet;
import fieldType.FieldTypeFiller;
import fieldType.FieldValidator;

public class CA3Test {

	private DefaultConfig columnasTipo;
	private DefaultConfig archivoHojaSimple; 
	
	private FieldTypeFiller instanciaErrorEdad;
	
	private FieldValidator validator;
	
	private SpreadSheet hojaSimpleErrEdad;
	private SpreadSheet hojaSimpleErrNombre; 

	@Before 
	public void setUp() throws IOException, EncryptedDocumentException, NotImplementedException, InvalidFormatException {
		columnasTipo = new DefaultConfig("columnasTipo.properties");
		
		archivoHojaSimple = new DefaultConfig("TestConfig.properties");	
		
		DataListStreamFactory lsf = new DataListStreamFactory();
		
		hojaSimpleErrEdad = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathErrorEdad"));
		instanciaErrorEdad = new FieldTypeFiller(columnasTipo);
		
		hojaSimpleErrNombre = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathErrorNombre"));
		
		validator = new FieldValidator(instanciaErrorEdad);		
	}
	
	
	@Test (expected = Exception.class)
	public void testA() throws Exception {
		validator.validate("Edad", DataType.STRING,instanciaErrorEdad.fill(hojaSimpleErrEdad));
	}	
	
	@Test (expected = Exception.class)
	public void testB() throws Exception {
		validator.validate("Nombre", DataType.INTEGER,instanciaErrorEdad.fill(hojaSimpleErrNombre));
	}	
}