package CAIteracion2;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import config.DefaultConfig;
import core.DataListStreamFactory;
import enums.DataType;
import excelModule.SpreadSheet;
import fieldType.FieldTypeFiller;

public class CA1Test {

	private DefaultConfig columnasTipo;
	private DefaultConfig archivoHojaSimple;

	private FieldTypeFiller instancia1;
	private FieldTypeFiller instancia3;

	private SpreadSheet hojaSimple;

	@Before
	public void setUp()
			throws IOException, EncryptedDocumentException, NotImplementedException, InvalidFormatException {
		columnasTipo = new DefaultConfig("columnasTipo.properties");
		archivoHojaSimple = new DefaultConfig("TestConfig.properties");

		DataListStreamFactory lsf = new DataListStreamFactory();

		hojaSimple = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathHojaSimple"));
		instancia1 = new FieldTypeFiller(columnasTipo);

	}

	@Test
	public void test1()
			throws EncryptedDocumentException, NotImplementedException, InvalidFormatException, IOException {
		Map<String, DataType> map = instancia1.fill(hojaSimple);
		assertEquals(DataType.STRING, map.get("Nombre"));
		assertEquals(DataType.INTEGER, map.get("Edad"));
	}

	@Test(expected = NoSuchElementException.class)
	public void test2()
			throws EncryptedDocumentException, NotImplementedException, InvalidFormatException, IOException {
		DataListStreamFactory lsf = new DataListStreamFactory();

		SpreadSheet hojaVacia = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathHoja_vacia"));
		instancia3 = new FieldTypeFiller(columnasTipo);
		instancia3.fill(hojaVacia);
	}

	// Se lee la hoja “columna 3 sin tipo” y al no reconocer
	// ningún tipo de dato para la columna 3 se lanza una excepción.
	@Test(expected = IOException.class)
	public void test3()
			throws EncryptedDocumentException, NotImplementedException, InvalidFormatException, IOException {
		DataListStreamFactory lsf = new DataListStreamFactory();
		SpreadSheet columnaTres = (SpreadSheet) lsf.create("xlsx", archivoHojaSimple.getElement("pathColumna3SinTipo"));

		instancia3 = new FieldTypeFiller(columnasTipo);
		instancia3.getDataType("columna3_sin_tipo", instancia3.fill(columnaTres));
	}

}
