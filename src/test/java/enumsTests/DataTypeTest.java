package enumsTests;

import static org.junit.Assert.*;

import org.junit.Test;

import enums.DataType;

public class DataTypeTest {

	@Test
	public void test() {
		assertEquals(DataType.STRING, DataType.parse("String"));
		assertEquals(DataType.INTEGER, DataType.parse("Integer"));
	}

}
