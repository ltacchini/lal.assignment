package CAIteracion1;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import core.Wrapper;
import coreTests.MockAlumno;

public class CA5Test {

	MockAlumno instancia0;
	MockAlumno instancia4; 
	
	@Before
	public void setUp() throws Exception {
		MockAlumno instanciaEjemplo = new MockAlumno();
		Wrapper wp = new Wrapper("TestConfig.properties", "resources/hojaSimple.xlsx/Hoja 1", "xls");
		try {
			instancia0 = (MockAlumno) wp.createInstances(instanciaEjemplo).get(0);
			instancia4 = (MockAlumno) wp.createInstances(instanciaEjemplo).get(2);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test
	public void testa() {
		assertEquals("A", instancia0.getNombre());
	}

	@Test
	public void testb() {
		assertEquals(3, instancia4.getEdad());
	}

}
