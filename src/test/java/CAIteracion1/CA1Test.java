package CAIteracion1;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import config.DefaultConfig;
import excelModule.SpreadSheet;

public class CA1Test {
	
	DefaultConfig archivoConfig;
	String pathArchivo;
	String pathHoja;
	SpreadSheet spreadSheet;
	
	@Before 
	public void setUp() throws IOException {
		archivoConfig = new DefaultConfig("TestConfig.properties");
		pathArchivo = archivoConfig.getElement("pathHojaSimple");
		pathHoja = archivoConfig.getElement("hoja1");
	}
	
	@Test
	public void hojaSimpleText() {
		assertEquals("resources/hojaSimple.xlsx/Hoja 1", pathArchivo);
	}
	
	@Test
	(expected = IOException.class)
	public void archivoNoEncontradoTest() throws EncryptedDocumentException, InvalidFormatException, IOException { 
		String pathError = archivoConfig.getElement("pathArchivoInexistente");
		spreadSheet = new SpreadSheet(pathError, pathHoja);
	}
	
	@Test
	(expected = IOException.class)
	public void archivoFormatoIncorrectoTest() throws EncryptedDocumentException, InvalidFormatException, IOException {
		String pathBlocDeNotas = archivoConfig.getElement("pathBlocDeNotas");
		spreadSheet = new SpreadSheet(pathBlocDeNotas, pathHoja);
	}
	
	@Test
	(expected = IOException.class)
	public void archivoIncorrectaTest() throws InvalidFormatException, IOException {
		String pathNoSoyUnExcel = archivoConfig.getElement("pathNoSoyUnExcel");
		spreadSheet = new SpreadSheet(pathNoSoyUnExcel, pathHoja);
	}
}
