package CAIteracion1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import config.DefaultConfig;
import core.DataListStreamFactory;
import excelModule.SpreadSheet;

public class CA2Test {
		
	private SpreadSheet instancia1;
	private SpreadSheet instancia2;

	@Before
	public void setUp() throws Exception {
				
		DefaultConfig archivoConfig = new DefaultConfig("TestConfig.properties");
		DataListStreamFactory lsf = new DataListStreamFactory();
		
		String urlArchivo = archivoConfig.getElement("pathHojaSimple");		
		instancia1 = (SpreadSheet) lsf.create("xlsx", urlArchivo);
		
		String urlArchivo2 = archivoConfig.getElement("pathExcelConHojas");
		instancia2 = (SpreadSheet) lsf.create("xlsx", urlArchivo2);
	}

	@Test
	public void getSheetsTest1() {
		assertEquals(3, instancia2.getSheets().size());
	}

	@Test
	public void sheetExistTest2() {
		assertFalse(instancia1.sheetExists("Hoja 2"));
		assertTrue(instancia1.sheetExists("Hoja 1"));
	}

	@Test
	public void getSheetAtTest1() {
		assertEquals("Hoja 1", instancia1.getSheetAt(0).getSheetName());
		assertEquals("Hoja 2", instancia2.getSheetAt(1).getSheetName());
	}

	@Test
	public void getSheetTest1() {
		assertEquals(instancia1.getSheetAt(0), instancia1.getSheet("Hoja 1"));
	}

}
