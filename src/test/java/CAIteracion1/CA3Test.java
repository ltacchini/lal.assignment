package CAIteracion1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import core.DataList;
import core.DataListStreamFactory;

public class CA3Test {

	/*
	 * Los primeros dos indices contienen a A1 y B1, los segundos a A2 y B2 Y así
	 * sucesivamente
	 */
	List<String> linesInRow;

	@Before
	public void setUp() throws Exception {
		String filePath = "resources/hojaSimple.xlsx/Hoja 1";
		String extension = "xls";
		DataListStreamFactory lsf = new DataListStreamFactory();
		Iterable<DataList> lines = lsf.create(extension, filePath);
		this.linesInRow = new ArrayList<>();
		for (DataList d : lines) {
			for (String s : d) {
				this.linesInRow.add(s);
			}
		}

	}

	@Test
	public void testa() {
		/*
		 * Se lee la celda A1 y se recibe el texto “Nombre", se lee la celda B1 y se
		 * recibe el texto “Edad”.
		 */
		String a1 = this.linesInRow.get(0);
		String b1 = this.linesInRow.get(1);

		assertEquals(a1, "Nombre");
		assertEquals(b1, "Edad");
	}

	public void testb() {
		/*
		 * Se lee la celda A2 y se recibe el texto “A”, se lee la celda B2 y se recibe
		 * el texto “1”.
		 */
		String a2 = this.linesInRow.get(2);
		String b2 = this.linesInRow.get(3);

		assertEquals(a2, "A");
		assertEquals(b2, "1");
	}

}
